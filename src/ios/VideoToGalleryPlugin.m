#import "VideoToGalleryPlugin.h"
#import <Cordova/CDV.h>

@implementation VideoToGalleryPlugin
@synthesize callbackId;

- (void)saveImageToGallery:(CDVInvokedUrlCommand*)command
{
    self.callbackId = command.callbackId;
    
    NSString* filePath = [command.arguments objectAtIndex:0];
    NSLog(@"File path in plugin:");
    NSLog(filePath);

    NSError* error = nil;
    NSURL* url = [NSURL URLWithString:filePath];
    NSData* imageData = [NSData dataWithContentsOfURL:url options:NSDataReadingUncached error:&error];
    
    UIImage* image = [[[UIImage alloc] initWithData:imageData] autorelease];    
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didImageFinishSavingWithError:contextInfo:), nil);

}

- (void)image:(UIImage *)image didImageFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error != NULL)
    {
        NSLog(@"ERROR: %@",error);
        CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:error.description];
		[self.commandDelegate sendPluginResult:result callbackId: self.callbackId];
    }
    else  // No errors
    {
        NSLog(@"IMAGE SAVED!");
        CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsString:@"Image saved"];
		[self.commandDelegate sendPluginResult:result callbackId: self.callbackId];
    }
}



- (void)saveVideoToGallery:(CDVInvokedUrlCommand*)command
{
    
    self.callbackId = command.callbackId;
    
    NSString* filePath = [command.arguments objectAtIndex:0];
    NSLog(@"File path in plugin:");
    NSLog(filePath);

    if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(filePath))
    {
        UISaveVideoAtPathToSavedPhotosAlbum(filePath, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
    }
    else
    {
        NSLog(@"UIVideoAtPathIsCompatibleWithSavedPhotosAlbum returned false");
    }

}

-(void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    // Was there an error?
    if (error != NULL)
    {
        // Show error message...
        NSLog(@"ERROR: %@",error);
        CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:error.description];
		[self.commandDelegate sendPluginResult:result callbackId: self.callbackId];
    }
    else  // No errors
    {
        NSLog(@"Video saved!");
        CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsString:@"Video saved!"];
		[self.commandDelegate sendPluginResult:result callbackId: self.callbackId];
    }
}

- (void)dealloc
{
    [callbackId release];
    [super dealloc];
}


@end
