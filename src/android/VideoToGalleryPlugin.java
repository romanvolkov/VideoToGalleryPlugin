package ru.romanvolkov.VideoToGalleryPlugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Calendar;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;


public class VideoToGalleryPlugin extends CordovaPlugin {
	public static final String ACTION = "saveVideoToGallery";

	@Override
	public boolean execute(String action, JSONArray data,
			CallbackContext callbackContext) throws JSONException {

		if (action.equals(ACTION)) {

			//data.optString(0); - это путь к файлу
			String filePath = data.optString(0);
			if (filePath.equals("")) // isEmpty() requires API level 9
				callbackContext.error("Missing filePath string");

			// Create the bitmap from the base64 string
			Log.d("VideoToGalleryPlugin", filePath);

				// Save the image
				File videoFile = saveVideo(filePath);
				if (videoFile == null)
					callbackContext.error("Error while saving image");

				// Update image gallery
				scanPhoto(videoFile);

				callbackContext.success(videoFile.toString());


			return true;
		} else {
			return false;
		}
	}

private void copyFile(File src, File dst) throws IOException{
    InputStream in = new FileInputStream(src);
    OutputStream out = new FileOutputStream(dst);

    // Transfer bytes from in to out
    byte[] buf = new byte[1024];
    int len;
    while ((len = in.read(buf)) > 0) {
        out.write(buf, 0, len);
    }
    in.close();
    out.close();
}

	private File saveVideo(String filePath) {
		File retVal = null;

		try {
			Calendar c = Calendar.getInstance();
			String date = "" + c.get(Calendar.DAY_OF_MONTH)
					+ c.get(Calendar.MONTH)
					+ c.get(Calendar.YEAR)
					+ c.get(Calendar.HOUR_OF_DAY)
					+ c.get(Calendar.MINUTE)
					+ c.get(Calendar.SECOND);

			String deviceVersion = Build.VERSION.RELEASE;
			Log.i("VideoToGalleryPlugin", "Android version " + deviceVersion);
			int check = deviceVersion.compareTo("2.3.3");

			File folder;
			/*
			 * File path = Environment.getExternalStoragePublicDirectory(
			 * Environment.DIRECTORY_PICTURES ); //this throws error in Android
			 * 2.2
			 */
			if (check >= 1) {
				folder = Environment
					.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

				if(!folder.exists()) {
					folder.mkdirs();
				}
			} else {
				folder = Environment.getExternalStorageDirectory();
			}
			File inputFile = new File(filePath);
			File videoFile = new File(folder, inputFile.getName() );

			copyFile(inputFile, videoFile);

			retVal = videoFile;


		} catch (Exception e) {
			Log.e("VideoToGalleryPlugin", "An exception occured while saving image: "
					+ e.toString());
		}
		return retVal;
	}

	/* Invoke the system's media scanner to add your photo to the Media Provider's database,
	 * making it available in the Android Gallery application and to other apps. */
	private void scanPhoto(File imageFile)
	{
		Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
	    Uri contentUri = Uri.fromFile(imageFile);
	    mediaScanIntent.setData(contentUri);
	    cordova.getActivity().sendBroadcast(mediaScanIntent);
	}
}
