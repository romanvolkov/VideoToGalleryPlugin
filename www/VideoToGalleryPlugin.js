  module.exports = {

      saveVideoToGallery: function(successCallback, failureCallback, filePath) {
          
          if (typeof successCallback != "function") {
              console.log("VideoToGalleryPlugin Error: successCallback is not a function");
          } else if (typeof failureCallback != "function") {
              console.log("VideoToGalleryPlugin Error: failureCallback is not a function");
          } else {
              console.log("VideoToGalleryPlugin: filePath = " + filePath);

              return cordova.exec(successCallback, failureCallback, "VideoToGalleryPlugin", 
                "saveVideoToGallery", [filePath]);
          }
      },


      saveImageToGallery: function(successCallback, failureCallback, filePath) {
          
          if (typeof successCallback != "function") {
              console.log("VideoToGalleryPlugin_saveImageToGallery Error: successCallback is not a function");
          } else if (typeof failureCallback != "function") {
              console.log("VideoToGalleryPlugin_saveImageToGallery Error: failureCallback is not a function");
          } else {
              console.log("VideoToGalleryPlugin_saveImageToGallery: filePath = " + filePath);

              return cordova.exec(successCallback, failureCallback, "VideoToGalleryPlugin", 
                "saveImageToGallery", [filePath]);
          }
      }
  };